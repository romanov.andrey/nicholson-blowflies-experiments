from jitcdde import jitcdde, y, dy, t
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import math
import datetime
import scipy.linalg as linal
from scipy import integrate as integ

matplotlib.use('Qt5Agg')

print('Program Started')

#the time T from the approximation scheme upto which solutions are obtained
finish = 15.

#Number of basis functions to use (the parameter N from the approximation scheme)
N = 10

def foo(
    P, delta
):
    #do not change tau = 1
    tau = 1

    #time steps at which solution is obtained
    step = 0.01

    #Tolerance for numerical integration
    TOL = 1e-6

    #max step in time for integration
    maxstep = 1e-5
    
    minD = 0.
    if P/(delta * math.e) <= 2.:
        minD = P * (1-P/(math.e * delta)) * np.exp(-P/(math.e * delta))
    else:
        minD = -P * math.e**(-2)

    #the parameter varkappa from the modified equations
    kappa = (minD + P) / 2.

    print(f'{delta=} {kappa=}')

    #the coefficient Lambda
    Lam = P - kappa

    LamM = 1./Lam

    delay_in_steps = int(tau / step)
    
    #the length of array corresponding to the time interval [0,T] according to step
    Tstep = int(finish/step)

    #Coefficient for the initial condition \psi_{\infty}
    PsiINF0 = np.sqrt(2) #(-1)**(m+1) * (m!)**(-0.5) * Btilde

    #initial condition \psi_{\infty}
    def histDelta0(t):
        return PsiINF0 * np.heaviside(t,1)
    
    #Omega from the aproximation scheme determining the interval [-Omega, Omega]
    OmegaBIG = 10.

    #Step for omega in the inverval [-Omega, 0]
    omega_step = 0.3
    omegas = np.arange(-OmegaBIG, 0, step=omega_step, dtype=np.float64)

    #the parameter nu0 from the frequency condition; to justify the stability we need nu0 > 0
    nu0 = 0.01

    rho = -nu0

    #vector field for the linear equation to integrate
    fLin = {
        y(i, t): -delta * y(i, t) + kappa * y(i, t - tau)
        for i in range(1)
    }

    def get_solution(DDE, history):
        DDE.past_from_function(history)
        DDE.set_integration_parameters(max_step=maxstep, first_step=maxstep, atol=TOL, rtol=TOL)
        DDE.adjust_diff()

        ss = []
        ts = []
        for time in np.arange(step, finish, step):
            ts.append(time)
            state = DDE.integrate(time)
            ss.append(state)
        ss = np.array(ss)
        return ts, ss

    tt = np.arange(0, finish, step=step)

    DDE = jitcdde(fLin, max_delay=tau)

    #computing x_{\infty}
    sINF, ssINF = get_solution(DDE=DDE, history=histDelta0)
    yyINF = []
    for i in reversed(range(0,delay_in_steps)):
        yyINF.append(histDelta0(-i*step))
    yyINF.extend(ssINF[:,0])
    yyINF = np.array(yyINF)

    print('Xinf computed')

    #if we want to print the graph of x_{\infty}
    #plt.plot(np.arange(-1, finish-step, step=step, dtype=np.float64), yyINF)
    #plt.tick_params(axis='both', which='major', labelsize=20)
    #
    #plt.grid()
    #plt.show()

    xAR = []

    #start computing solutions x_{k}
    for k in range(0, N+1):
        def histOrtKR(t):
            return np.cos(k*2*np.pi*t) #tau = 1

        def histOrtKIM(t):
            return np.sin(k*2*np.pi*t) #tau = 1

        s11, ss11 = get_solution(DDE=DDE, history=histOrtKR)
        s12, ss12 = get_solution(DDE=DDE, history=histOrtKIM)
        yy11 = []
        for i in reversed(range(0,delay_in_steps)):
            yy11.append(histOrtKR(-i*step))
        yy11.extend(ss11[:,0])
        yy11 = np.array(yy11)
        yy12 = []
        for i in reversed(range(0,delay_in_steps)):
            yy12.append(histOrtKIM(-i*step))
        yy12.extend(ss12[:,0])
        yy12 = np.array(yy12)

    #if we want to print graphs for some solutions
    #    plt.plot(np.arange(-1, finish-step, step=step, dtype=np.float64), yy11)
    #    plt.plot(np.arange(-1, finish-step, step=step, dtype=np.float64), yy12)
    #    plt.tick_params(axis='both', which='major', labelsize=20)
    #
    #    plt.grid()
    #    plt.show()

        xAR.append(yy11 + 1j*yy12)

        #above we have added x_{k}; now we add x_{-k}
        if k > 0:
            xAR.append(yy11 - 1j*yy12)

        print('X[', k, '] computed')

    #making an array of xAR; so xAR = [x_{0}, x_{1}, x_{-1}, x_{2}, x_{-2}, ...]
    xAR = np.array(xAR)

    print('Solutions computed')

    theta = np.array([-tau + step * i for i in range(delay_in_steps)])
    Tinterval = np.array([0 + step * i for i in range(Tstep)])
    basisConj_dict = {}

    for j in range(-N, N+1):
        #define sqrt(m) * (U^{1}_{j})*; the coefficient sqrt(m) is taken into account at the end of calc_matrix(p)
        def ConjOrtBase(t):
            return np.exp(-1j * j * 2 * np.pi * t) #tau=1 so we dont normalize
        basisConj_dict[(j)] = ConjOrtBase(theta)

    print('Basis vectors computed')

    #calculating matrix W_{T,N}(p)
    def calc_matrix(p):
        M = np.empty((2*N+1, 2*N+1),dtype=complex)
        #print('Start computation for p=',p)
        #theta = np.array([-tau + step * i for i in range(delay_in_steps)])
        
        aExp = np.exp(-p * Tinterval)
        for k in range(-N, N+1):
            counter = -1
            if k > 0:
                num = 2*k-1
            else:
                num = -2*k
            ans = np.array(xAR[num])
            YgK = []
            for s in theta:
                counter = counter + 1
                intGt = 0.5 * ( ans[ : -delay_in_steps + 1] * yyINF[counter : Tstep + counter] - ans[counter : Tstep + counter] * yyINF[ : -delay_in_steps + 1] )
                y = aExp * intGt
                YgK.append(integ.simpson(y,x=Tinterval))
            YgK = np.array(YgK)
            for j in range(-N, N+1):
                M[j+N,k+N] = np.sqrt(2) * integ.simpson( YgK * basisConj_dict[(j)],x=theta) # sqrt(2) = m / sqrt(m), where m from (AS.3.3) and 1/sqrt(m) from the definition of U^{1}_{k}
        #print('Matrix computed')
        return M

    def calc_freq(p):
        M = calc_matrix(p)
        svdM = linal.svd(M,compute_uv=False)
        w = svdM[0]
        #print('Largest singular value computed: ', w)
        #print('Cond number: ', svdM[0]/svdM[2*N])
        w = np.real(w)
        return w

    def run_rho(rho,omegas):
        flag = True
        for omega in omegas:
            if calc_freq(1j * omega + rho) >= LamM:
                flag = False
                break
        return flag
    
    return run_rho(rho, omegas)

fig, ax = plt.subplots(1, figsize=(4, 4))
plt.rcParams['text.usetex'] = True

#xs stays for the interval of P, ys stays for the interval of delta
xs = np.arange(1.5, 5, 0.025)
ys = np.arange(0.05, 5, 0.025)

x0, y0 = np.meshgrid(xs, ys)
mask0 = (np.exp(y0) - 1) * (x0 / y0 - 1) < 1

x1, y1 = np.meshgrid(xs, ys)
mask1 = y1 * (np.log(x1 / y1) - 1) * np.exp(y1 + 1) > 1


xx, yy = [], []
prev_y_index = 0
for _x in xs:
    y_index = prev_y_index
    while y_index < len(ys) and not foo(_x, ys[y_index]):
        print(f"Calculating for {(_x, ys[y_index])=}")
        y_index += 1
    if y_index < len(ys):
        xx.append(_x)
        yy.append(ys[y_index])
    prev_y_index = y_index

#Save computed points into a file to plot it in other program
with open(f'{__file__}_{N=}_{finish=}_out.txt', 'w') as fout:
    fout.write(','.join(map(str, xx)))
    fout.write('\n')
    fout.write(','.join(map(str, yy)))

plt.scatter(x0[mask0], y0[mask0], alpha=0.9, marker='*', label=r'$(e^{\delta} - 1) (P / \delta - 1) < 1$')
plt.scatter(xx, yy, alpha=0.5, label='our calc')
#plt.scatter(x1[mask1], y1[mask1], marker='x', alpha=0.9, color='black', label=r'$\delta (\ln(P/\delta) - 1) e^{\delta + 1} > 1$')

ax.set_xlabel("P", fontsize=20)
ax.set_ylabel("delta", fontsize=20)
plt.tick_params(axis='both', which='major', labelsize=20)

plt.legend(loc='best')
plt.grid()
plt.show()

