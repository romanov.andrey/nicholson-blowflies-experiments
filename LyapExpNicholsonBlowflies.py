import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from jitcdde import jitcdde, y, dy, t, jitcdde_lyap
from symengine import cos, sin, exp
import matplotlib as mpl
import numpy as np
from scipy.stats import sem
import sympy as sm


mpl.rcParams['legend.fontsize'] = 10

#initial history for the trajectory over which to compute Lyapunov exponents
history1 = [0.2+0.00001]

lyapunov_exsnonents = []

#Here A is a free parameter if we need to compute Lyapunov exponents for several system parameters
for A in np.arange(0.073, 0.0731, 0.001):
    print(A)

    #parameters of the Nicholson blowflies model
    tau = 1.
    delta = 9.5
    a = 1.
    P = 250.


    def g(y):
        return P * y * sm.exp(-a * y)

    f = {
        y(i, t): -delta * y(i, t) + g(y(i, t - tau))

        for i in range(1)
    }

    step = 0.01
    max_step = 0.0001

    #time upto which compute Lyapunov exponents
    finish = 5000

    delay_in_steps = int(np.ceil(tau / step))
    two_pi_in_steps = int(np.ceil(2 * np.pi / step))


    DDE = jitcdde(f, max_delay=tau)
    DDE.constant_past([0.2])
    DDE.set_integration_parameters(max_step=max_step, first_step=max_step, atol=1e-6, rtol=1e-6)
    DDE.adjust_diff()

    ts, ss = [], []
    for time in np.arange(step, finish, step):
        ts.append(time)
        state = DDE.integrate(time)
        ss.append(state[0])

    ts = np.array(ts)
    ss = np.array(ss)
    _ts = (ts - ts[-1])[1:]
    dts = np.diff(_ts)
    dx = np.diff(ss[1:]) / dts
    N = len(dts)

    n_lyap = 5
    DDE_lyap = jitcdde_lyap(f, n_lyap=n_lyap, max_delay=tau)
    for i in range(N-delay_in_steps, N):
        DDE_lyap.add_past_point(ts[i], ss[i], dx[i])
    DDE_lyap.set_integration_parameters(max_step=max_step, first_step=max_step, atol=1e-6, rtol=1e-6)
    DDE_lyap.adjust_diff()

    T = ts[-1]
    ts, ss, ls, ws = [], [], [], []
    finish = 5_000
    for time in np.arange(T + step, T + finish, step):
        ts.append(time)
        state, lyap, weight = DDE_lyap.integrate(time)
        ss.append(state)
        ls.append(lyap)
        ws.append(weight)

    ls = np.array(ls)
    ws = np.array(ws)

    ans = []
    for i in range(n_lyap):
        L = np.average(ls[400:, i], weights=ws[400:])
        stderr = sem(ls[400:, i])
        print("%i. Lyapunov exponent: % .4f +/- %.4f" % (i+1, L, stderr))
        ans.append(L)
    lyapunov_exsnonents.append(ans)

    ss = np.array(ss)
    yy = ss
    fig, ax = plt.subplots(1, figsize=(4, 4))
    ax.tick_params(labelsize=15)
    plt.scatter(yy[delay_in_steps:], yy[:-delay_in_steps], color='red', s=2)
    plt.xlim([-1, 1])
    plt.ylim([-1, 1])
    plt.savefig(f'amp_{round(A, 4)}.png', dpi=400)

print(lyapunov_exsnonents)


