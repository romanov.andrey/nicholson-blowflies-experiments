import numpy as np
from scipy import stats
from scipy.special import lambertw

# for plots
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt

from itertools import combinations

#parameter tau from the Nicholson blowflies model
tau = 1.
#parameter delta from the Nicholson blowflies model
delta = 9.5

xp = 0


def do_regressions(xsn, ysn):
    print('Regressions')

    slopeN, interceptN, _, _, _ = stats.linregress(xsn,ysn)
    print('slopeNEW = ', slopeN)

    print('Plotting')

    plt.scatter(xsn, ysn)
    plt.plot(xsn, slopeN * xsn + interceptN,                       color='red',   label='         linear')
    plt.legend()
    plt.show()

P_step = 5
#interval of P from the Nicholson blowflies model over which to compute
Pes = np.arange(250, 10000., step=P_step, dtype=np.float64)

LyapDim = []

for Pe in Pes:
    flag = False
    n = 0
    sum = 0
    kappa = 0
    a = -delta
    beta = Pe * (1 - xp) * np.exp(-xp)
    b = beta
    while not flag:
        lam = (lambertw(b*np.exp(-a*tau)*tau, -n) + a*tau)/tau
        if np.real(lam) + sum > 0.:
            sum = sum + np.real(lam)
            if np.imag(lam) == 0:
                kappa = kappa + 1
            n = n + 1
        else:
            flag = True
    LyapDim.append(2*n - kappa)

do_regressions(Pes,LyapDim)

